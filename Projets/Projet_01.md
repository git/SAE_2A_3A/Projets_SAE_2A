# Tableau de bord

Client : [Laurent GIRAUD](mailto:laurent.giraud@uca.fr)

## Contexte de la SAÉ

Les étudiant.e.s de 2<sup>ème</sup> année doivent obligatoirement faire un stage. Certain.e.s le réalisent en alternance et l'ont trouvé dès la fin de la première année.

La recherche peut être longue et fastidieuse, parfois décourageante. On envoie beaucoup de CV et de LM, et on n'obtient pas toujours facilement un entretien de recrutement.

L'analyse de vos échecs montre que vous ne suivez pas toujours les conseils des coachs et RH qui viennent vous faire des cours en expression-communication, dans le cadre des TRE (techniques de recherche d'emploi) : analyse superficielle d'offres de stage, envoi de CV identiques à des entreprises différentes, absence de profilage en fonction de l'entreprise, relecture aléatoire de documents fautifs, modifications faites au fil du temps sans que vous gardiez la moindre trace de vos expériences, etc.

## Sujet de la SAÉ

La SAÉ consiste à créer une application, avec un site web et une application mobile, qui permettra aux étudiant.e.s de lister, d'archiver, et de conserver les documents dans un espace numérique dédié. Ils/Elles pourront stocker les annonces de stage, les CV et LM envoyés, les réponses reçues, remplir un compte rendu d'entretien de recrutement.

Cela consiste donc à créer un outil documentaire et de travail, qui puisse donner aux étudiant.e.s une vision panoramique de leur recherche, sous forme de « tableau de bord » (d'où le titre de la SAÉ), et donc de modifier leur stratégie de recherche en fonction des expériences passées.

On pourra envisager une extension du sujet à destination des alternant.e.s en imaginant comment l'adapter aux premières années. Mais il sera imaginé pour servir aux FI comme aux FA, sans distinction de fonctionnement.

L'enseignant référent de communication du département informatique Laurent Giraud en sera l'administrateur, et pourra consulter les travaux, échanger par mail avec les étudiant.e.s selon leurs demandes, et apporter son aide.

## Attendus techniques possibles de la SAÉ

L'application correspondra pour ses outils techniques aux langages, frameworks, etc., appris en cours, selon des technologies diverses à définir (compétence 1 = développer).

Le site web sera dynamique, « responsive », en lien avec les technologies apprises en BUT1 et 2.

Un soin particulier sera accordé aux méthodes de programmation, et l'application comme le site web devront correspondre aux normes de qualité et de développement en POO, ainsi qu'aux principes d'optimisation pour rendre l'application aussi économe que possible en ressources, en lien avec votre pratique des réseaux. (compétences 2 et 3). La base de données se fera avec les techniques apprises en cours depuis le S1 (SQL, autres, compétence 4).

Le travail d'équipe (compétence 6) sera aussi sollicité avec des réunions dont la fréquence sera à moduler selon vos disponibilités et la charge de travail en cours, dans des rencontres avec le client.

