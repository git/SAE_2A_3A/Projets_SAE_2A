# Let's go!

Client : [Michelle CONRY-FIDONE](mailto:michelle.conry-fidone@uca.fr)

## Description

J'ai besoin d'un outil ludique et pédagogique pour aider les étudiants à travailler leur vocabulaire en anglais.

Avant, j'utilisais *Quizlet* mais depuis l'année dernière ce site est devenu payant pour l'étudiant, donc plus possible de l'exploiter en cours. J'aimerais donc un outil similaire.

Sur le site original, il y a plusieurs types d'exercices de vocabulaire et des jeux pour que les étudiants s'entrainent.

Venez me voir si vous souhaitez plus d'information (vous pouvez également aller sur le site [*Quizlet*](https://quizlet.com/fr) pour avoir une meilleure idée du projet).

