# Citations autour de la Fantasy

Client : [Matthieu RESTITUITO](mailto:matthieu.restituito@uca.fr)

## Description

Le site et l'application à développer dans ce projet viseront à recenser et mettre à disposition des citations provenant de genre divers et de sources variées (cinéma, littérature, etc.)

Les fonctionnalités attendues sont à minima :

- Citations validées avec source valide ou exemple;
- Moteur de recherche avec critères divers;
- Gestion des langues en fonction des médias;
- Affichage par page et par thématique;
- Favoris et votes;
- Partie ludique (à définir).

