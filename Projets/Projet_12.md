# Le réseau social des bricoleurs

Client : [Cédric BOUHOURS](mailto:cedric.bouhours@uca.fr)

## Description

Le réseau social des bricoleurs : un réseau social, mais parfaitement adapté aux bricoleurs (des fois, les mains sont propres, des fois, les mains sont sales, et pourtant, il faut pouvoir utiliser ce réseau).

Il faudra au minimum :

- Photos
- Vidéos
- Tutos
- Tutos live
- Conseils en peer2peer

… et tout plein d'autres choses auxquelles je n'ai pas encore pensé. Le tout empaqueté dans des applis mobiles et des applis web et bien sûr, avec un back-office pour l'administrateur du réseau.

