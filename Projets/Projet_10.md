# Site support du cours Muscu Math

Client : [Adrien WOHRER](mailto:adrien.wohrer@uca.fr)

Dans le cadre de l'activité « Musculation mathématique », les étudiants effectuent des séries de petites questions mathématiques, sur différents thèmes, afin d'évaluer/entraîner leurs automatismes de base en math. 

Le but de ce projet est de développer un site support pour cette activité, dans lequel les étudiants pourront noter leur progression.

## Description

Le site devra avoir les caractéristiques suivantes :

#### Fonctionnalités pour les étudiants

- chaque étudiant dispose d'un compte + mot de passe (il peut utiliser un pseudo);
- après chaque série de questions, l'étudiant rentre son score réalisé dans l'application : numéros des 10 questions essayées, et quelles questions ont été réussies;
- le site garde en mémoire les scores de chaque étudiant sur l'ensemble des sessions passées;
- l'étudiant peut visualiser ses scores passés, sur chaque thème, et évaluer sa progression.

#### Fonctionnalités pour le professeur

- l'enseignant peut récupérer l'ensemble des résultats stockés sur l'application, anonymisés (on ne transmet pas les pseudos des étudiants);
- l'enseignant peut faire des statistiques basiques :
  + pourcentage de réussite sur une question donnée;
  + évolution du niveau de la classe, sur un thème donné, au fil des séances.

Le site aura pour vocation principale d'être utilisé sur téléphone. Une fois terminé et fonctionnel, le site devra être hébergé sur un serveur de l'IUT.

