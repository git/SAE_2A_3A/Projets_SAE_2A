# Fake news

Client : [Laurent GIRAUD](mailto:laurent.giraud@uca.fr) et [Maxime PUYS](mailto:maxime.puys@uca.fr)

## Contexte de la SAÉ

La multiplication des « fake news » rend difficile l'interprétation de l'information. Le grand public a souvent besoin qu'on l'aide à repérer les informations malveillantes, tronquées, ou fausses. Le domaine de la lutte d'influence passe de loin les domaines de la politique, de la défense, pour s'étendre au commerce, à la propriété intellectuelle et industrielle, la réputation d'autrui, etc. Les mauvaises informations entraînent une simplification du débat public dans les démocraties, un appauvrissement des échanges dialectiques, et l'émergence d'une forme nouvelle de violence orale, écrite et idéologique. Ces « fakes » sont un terreau fertile pour les menées complotistes et conspirationnistes.

## Sujet de la SAÉ

Cette SAÉ a déjà été proposée à une équipe en 2023-2024. Vos camarades ont déjà développé un site web et une application mobile, comme il était demandé dans la feuille de route. Le site et l'application consistaient à concevoir et développer une application et un site web qui recensent les « infox » à partir de liens, de bases de données, à les analyser et à les démystifier par des articles écrits, des vidéos, des podcasts, des critiques d'images fixes ou animées, adossés à une base de données qui les classeraient en fonction de leur thématique et de leurs rapports aux lois qui les sanctionnent. Des exemples factices ont été rendus accessibles en ligne et sur l'application, y compris grâce à des applications mobiles, pour consulter, publier, modifier selon des niveaux d'accréditation différents : simple internaute lecteur et consultant, contributeur agréé, administrateur. Le principe était donc le libre accès, la construction d'une communauté de rédacteurs et pédagogues de l'information contemporaine. Mais la tâche n'a pas été menée à bout, et le sujet réclamerait des prolongements techniques, dont voici quelques exemples :

- Faire en sorte qu'un utilisateur enregistré puisse surligner des passages de l'article pour ajouter contexte + sources;
- Up/down vote des autres utilisateurs sur les contextes + sources;
- Calcul du pourcentage de l'article vérifié;
- Présentation des articles les plus vérifiés en premier ?
- Classification des sources (ex: Le Monde > Facebook) ?

De plus, la rédaction d'un article, sur la base d'erreurs corrigées, n'a pu être mise en place l'année précédente. L'équipe serait en charge de remplir le site web avec un exemple authentique.

## Attendus techniques possibles de la SAÉ

L'application correspondra pour ses outils techniques aux langages, frameworks, etc., appris en cours, selon des technologies diverses à définir (compétence 1 = développer).

Le site web sera dynamique, « responsive », en lien avec les technologies apprises en BUT1 et 2.

Un soin particulier sera accordé aux méthodes de programmation, et l'application comme le site web devront correspondre aux normes de qualité et de développement en POO, ainsi qu'aux principes d'optimisation pour rendre l'application aussi économe que possible en ressources, notamment au niveau des réseaux, et des conteneurs qui auront pour fonction de rendre le code exécutable de manière efficace et de le connecter à n'importe quelle infrastructure (Docker ?) (compétences 2 et 3). La base de données se fera avec les techniques apprises en cours depuis le S1 (SQL, autres, compétence 4). Cette base de données requerra dans sa construction une organisation qui permettra de classer les articles selon les domaines du droit auxquels ils se rapportent : code pénal, code électoral, code monétaire et financier, code de la consommation, etc.

Outre des connaissances en gestion de projet, le sujet de SAÉ mettra en jeu les apprentissages des cours de droit des TIC (compétences 4 et 5).

Enfin, le travail d'équipe (compétence 6) sera aussi sollicité. Il sera bien que l'équipe d'étudiants soit elle-même contributrice pour un exemple de publication, afin de constituer une « démo ». Le client sera l'aide pour les guider.

