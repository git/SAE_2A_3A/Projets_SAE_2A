# Qwirkle

Client : [Pascal LAFOURCADE](mailto:pascal.lafourcade@uca.fr)

<img src="https://www.espritjeu.com/upload/image/qwirkle-p-image-89697-grande.jpg" width="50%"/>

Concevoir une application Web et Mobile pour le jeu Qwirkle.


## Description

Qwirkle se compose de 108 tuiles en bois avec 6 formes de 6 couleurs différentes. Chaque joueur essaye de marquer le maximum de points en constituant des lignes de tuiles ayant une couleur ou une forme en commun.

Le but est de réaliser des lignes de tuiles ayant la même forme ou la même couleur (par exemple, une ligne de tuiles jaunes ou une ligne de tuiles carrées).

- Chaque joueur commence en piochant 6 tuiles au hasard;
- On peut jouer autant de tuiles que l'on veut dans une ligne (1, 2 voire 6 tuiles d'un coup);
- On en repioche autant que nécessaire pour avoir toujours 6 tuiles;
- À chaque coup, le joueur marque autant de points qu'il y a de tuiles dans la ligne (par exemple, si je joue la 3ème et la 4ème tuile dans une ligne, je marque 4 points);
- Compléter une ligne de six tuiles rapporte 6 points plus un bonus supplémentaire de 6 points (soit 12 points !);
- Quand une tuile complète deux lignes (une horizontale et une verticale), on marque les points dans les deux sens;
- Celui qui a le plus de points à la fin de la partie a gagné.

Même si les règles sont simples, la victoire passe par une audace tactique et une stratégie bien élaborée.

