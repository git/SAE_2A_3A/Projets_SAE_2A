# Dashboard de suivi des conventions de stage

Client : [Christelle MOTTET](mailto:christelle.mottet@uca.fr)

## Description

Tous les étudiants en BUT informatique 2A et 3A doivent effectuer un stage en entreprise, certains font même une alternance. Chacun nécessite la mise en place d'une convention dont il faut suivre l'avancement : saisie des informations, vérifications, envois de mails aux différents protagonistes impliqués, …
C'est long et répétitif, et pour l'instant je dois tout faire manuellement :sweat:

J'en appelle donc à vos compétences en informatique pour m'aider et automatiser autant que possible cette procédure !

J'aimerais donc une application web et mobile (de type *dashboard*) qui me permette :

- D'être notifiée qu'une convention a été remplie par l'étudiant;
- De vérifier que les informations entrées sont correctes;
- D'envoyer automatiquement un mél de demande de signatures aux protagonistes (étudiant, entreprise, professeur encadrant, chef de département).

Aussi, les soutenances demandent un gros travail d'organisation, notamment dans l'envoi des invitations aux entreprises. J'aimerais donc que l'application que vous développerez me permette :

- D'envoyer automatiquement ces mails aux entreprises une fois le planning établi;
- D'extraire, pour les alternants, les informations nécessaires depuis un fichier Excel fourni par ma collègue.

Évidement, l'application devra être ergonomique et intuitive (il ne faudrait pas perdre du temps à chercher une fonctionnalité dans une application faite pour en faire gagner) et surtout… *jolie*, ben oui, c'est quand même plus agréable de travailler dans ces conditions :blush:

