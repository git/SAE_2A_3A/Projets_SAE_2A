# À table !

Client : [Michelle CONRY-FIDONE](mailto:michelle.conry-fidone@uca.fr)

## Description

J'aimerais une application/site web pour m'aider à planifier les repas pour les petits, voir toute la famille, afin d'avoir des menus variés et pouvoir planifier les ingrédients à acheter en amont. J'aimerais pouvoir avoir accès à des recettes et pouvoir les adapter au nombre de personnes/repas … 

Par ailleurs, ma fille est allergique aux œufs et je dois fournir ses repas à la crèche. J'aimerais pouvoir communiquer le menu de jour avec la crèche et, qu'éventuellement, ils puissent fournir certaines informations également.

Je suis sûre que vous allez avoir plein d'idées pour m'aider :-) N'hésitez pas à me contacter si vous avez besoin de plus d'information.

