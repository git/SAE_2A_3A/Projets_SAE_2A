# Chess2BUT

![](https://scontent-cdg4-3.xx.fbcdn.net/v/t1.6435-9/84311181_117892106425054_7240574292730052608_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=cc71e4&_nc_ohc=WsVMquQgOLYQ7kNvgETq1_w&_nc_ht=scontent-cdg4-3.xx&oh=00_AYDdCqcIKsg04RCLgjW94OuBWcFo4hUJhUM5a4_ch6V0LA&oe=66FD81B9)

## Développement d'un Système de Jeu d'Échecs

Client : [Redha TAGUELMIMT](mailto:redha.taguelmimt@gmail.com)

## Description

L'objectif est de développer un jeu d'échecs complet, avec une interface graphique web et mobile, des règles de jeu bien implémentées, et des fonctionnalités avancées (historique, exploration des coups joués, …). Le jeu doit permettre de jouer contre un autre joueur humain en local, ou de l'affronter en réseau à distance. Les étudiants doivent également implémenter un système de sauvegarde et de chargement de partie.

En fonction de l'avancement, il sera possible d'implémenter une fonctionnalité de jeu permettant à un joueur d'affronter une IA.

https://philippmuens.com/minimax-and-mcts

