# Cuisinote :

Client : [Anaïs DURAND](mailto:anais.durand@uca.fr)

Application permettant d'aider à la gestion de ses repas.

## Description

L'application doit permettre à l'utilisateur de gérer ses recettes de cuisine, d'en importer depuis des sites comme marmiton, de les trier par catégorie, de rechercher une recette par ingrédient, etc.

À partir d'une sélection de recettes, l'application doit permettre de générer une liste de courses. Lorsque l'utilisateur fait ses courses, cette liste doit pouvoir être mise à jour (par exemple, cocher les ingrédients déjà pris), y compris par plusieurs utilisateurs.
