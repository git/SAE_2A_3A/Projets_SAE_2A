# SQL Check

Client : [Anaïs DURAND](mailto:anais.durand@uca.fr)

Application permettant aux étudiants de tester et vérifier leurs requêtes lors des TPs de base de données.

## Description

Lors d'un TP de bases de données, les étudiants pourront, pour chaque question, vérifier leur requête en remplissant le champ correspondant.
L'application devra vérifier la requête, notamment en comparant son résultat avec celui de la correction fournie par les enseignants administrant le site.

L'application devra également permettre de modifier les sujets de TPs et de générer les PDF correspondants.
