# Application de suivi de formation pour sapeurs-pompiers

Client : [Carine SIMON](mailto:carine.simon@uca.fr)

## Contexte

Le corps des sapeurs-pompiers du Puy-de-Dôme organise tout au long de l'année des formations diplômantes ou certifiantes pour les pompiers professionnels ou volontaires du département.

Un pompier ne peut suivre une formation que s'il y a été inscrit au préalable. Tout au long de la formation ou au plus tard en fin de formation (selon le cas), le formateur doit consigner sur une fiche propre à chaque participant les compétences (concernées par la formation) qui sont validées, en cours d'acquisition ou non acquises.

À l'issue de la formation et selon les compétences acquises, la formation est alors validée ou non.

Actuellement les fiches sont au format papier, ce qui entraîne quelques problèmes logistiques.

## Objectif

L'idée est de créer une application mobile pour que le formateur ait, par le biais d'une tablette ou d'un téléphone, accès à la fiche associée à chaque candidat et puisse saisir des données directement. Une fois enregistrée, une fiche récapitulative pourrait être accessible pour un autre formateur qui interviendrait sur un autre module de la formation ou pour le responsable pédagogique.

## Exigences

Cette application devra permettre, a minima :

- à une personne du service formation de saisir ou importer les données de chaque formation (nom de la formation, type de formation (incendie, secours à personne, secours routier, …), la date de la formation, la liste des inscrits, la liste des formateurs, …);
- au formateur de saisir des données liées à la validation ou non de compétences concernées par la formation;
- au responsable pédagogique d'avoir une vue récapitulative des compétences acquises ou non pour chaque inscrit à la formation.

