# Gestion matériel IUT

Client : [Marc CHEVALDONNÉ](mailto:marc.chevaldonne@uca.fr)

Création d'une application mobile multiplateformes (Android/iOS) et si possible web permettant de gérer le matériel du département informatique et les emprunts.

## Description

Les étudiants pourront :

- consulter la liste du matériel qu'ils ont emprunté;
- consulter la liste du matériel qu'il est possible d'emprunter et pourront formuler une demande;
- recevront des rappels quand la date limite de rendu approchera ou sera passée.

Les gestionnaires de matériel pourront :

- valider une demande d'emprunt;
- valider un retour de matériel;
- ajouter du nouveau matériel à emprunter;
- consulter la liste du matériel emprunté et les personnes ayant emprunté le matériel.

Les super-admins pourront :

- donner ou enlever des droits de gestionnaires à des utilisateurs;
- retirer du matériel de l'inventaire.

