# Jeu de couverture par chemins dans les graphes

Client : [Florent FOUCAUD](mailto:florent.foucaud@uca.fr)

Le but du projet est de développer une application permettant de jouer au jeu suivant. 

## Description

Le plateau de jeu est un graphe non-orienté (par exemple le graphe correspondant à une grille 5x5, mais tout autre graphe peut convenir). Tour à tour, les joueurs sélectionnent un plus court chemin dans le graphe, entre deux sommets de leur choix. Si le chemin sélectionné n'est pas un chemin le plus court, le tour du joueur est annulé et on passe au joueur suivant. Sinon, pour chaque sommet nouvellement couvert par le chemin (c'est-à-dire qu'il n'était pas encore couvert par un chemin précédemment construit), le joueur marque un point. Lorsque tous les sommets ont été couverts, le jeu se termine. Le vainqueur est celui qui a le plus de points.

L'application devra permettre de jouer à ce jeu à plusieurs joueurs (sur le même terminal dans un premier temps, éventuellement en se connectant à un serveur en ligne). On peut également imaginer la conception d'une IA pour un mode de jeu en solo. D'autres fonctionnalités peuvent être envisagées selon le temps : variantes du jeu, création de graphes, leaderboard, etc.

