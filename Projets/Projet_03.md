# SQLuedo

Client : [Anaïs DURAND](mailto:anais.durand@uca.fr)

Jeu pour découvrir le SQL en résolvant des enquêtes.

## Description

Le joueur doit résoudre des enquêtes policières grâce à une base de données. Ce jeu doit permettre de découvrir le langage SQL en utilisant des blocs (façons *Scratch* ou *Blocky*) pour écrire des requêtes SQL afin d'obtenir les indices pour résoudre l'enquête. Un exemple d'enquête est disponible ici : 
https://edenmaths.gitlab.io/bcpst1/2022_23/INFORMATIQUE/12_sql/#sql-murder-mystery

L'application devra intégrer des outils d'administration aidant à la création de nouvelles enquêtes (génération automatique de personnages, …).

