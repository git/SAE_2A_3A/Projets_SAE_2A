# Gestion de locations courte durée

Client : [Neddra CHATTI](mailto:neddra.chatti@uca.fr)

## Description

L'objectif de ce projet est de créer un site internet de réservation pour de la location courte durée ainsi qu'une application mobile.

J'ai besoin d'un site internet qui remplisse plusieurs missions : réservation d'un bien en location courte durée qui soit synchronisé avec les calendriers de reservation Airbnb, Booking et Leboncoin.

Ce site a vocation de me permettre de :

- communiquer sur le bien à louer (visuel, messagerie dont messagerie automatique, règlement intérieur);
- permettre à des personnes de réserver le bien (sécurité importante, …);
- permettre à un co-hôte (conciergerie, femme de ménage) d'accéder au calendrier du bien et le gérer avec moi.

… et bien d'autres fonctionnalités.

