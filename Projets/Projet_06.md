# Dékal

Client : [Pascal LAFOURCADE](mailto:pascal.lafourcade@uca.fr)

![](https://www.espritjeu.com/upload/image/dekal-p-image-92909-grande.jpg)

Concevoir une application Web et Mobile pour le jeu Dékal.


## Description

Dans le jeu Dékal, vous allez tenter d'agencer au mieux vos cartes afin de marquer le moins de points possible. Mais attention, vos adversaires ou le manque d'anticipation peuvent désorganiser toute votre stratégie !

Vous démarrez avec 16 cartes faces cachées que vous disposez en une grille carrée de 4 x 4 cartes. Vous n'allez pas jouer avec les cartes restantes. Désormais, c'est parti pour 16 tours de jeu !

À chaque tour, vos adversaires et vous choisissez une carte face cachée dans votre propre grille et la retournez face visible, au milieu de la table, en même temps.
En commençant par le premier joueur et en continuant dans le sens des aiguilles d'une montre, chaque joueur choisit ensuite une des cartes du milieu de la table, qu'il place dans sa grille.
Pour effectuer ce placement, il devra pousser (faites glisser) la carte dans une ligne ou une colonne pour remplir un espace vide et reconstruire la grille de 4 par 4. Il n'est donc pas possible de faire glisser les cartes en diagonale, ni de placer une carte directement dans un emplacement vide.

Le jeu se termine lorsque toutes les cartes ont été révélées et jouées, et il faut alors compter les scores. Toutes les cartes orthogonalement adjacentes portant le même numéro s'annulent et sont retirées du jeu. Chaque joueur compte les valeurs des cartes restantes dans sa grille. Le joueur avec le score le plus bas remporte la partie.

