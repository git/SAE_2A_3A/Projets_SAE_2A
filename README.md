# SAÉ de 2A

## Règles de fonctionnement

Voici les règles qui encadrent la réalisation de vos SAÉ pendant votre deuxième année :

- Les deux SAÉ sont fusionnées autour du même projet, soit 165h pour les FI et 100h pour les FA :
  + Les FI travaillent toute l'année en suivant les créneaux à l'emploi du temps
  + Les FA travaillent comme les FI (en suivant les créneaux de l'emploi du temps) mais auront des périodes concentrée pendant les vacances de la Toussaint (6h par jour pendant 4 jours) et les vacances de février (6h par jour pendant 5 jours).
- Un cadre de projet a été défini pour structurer l'harmonie avec les ressources (voir ci-dessous).
- Les enseignants proposant les projets ne seront pas les tuteurs du projet mais les *clients*. Leur rôle sera donc uniquement de s'assurer du respect du cœur de cible.
- Au-delà de l'architecture générale, le cadre définit quelles ressources sont concernées dans le projet (voir ci-dessous).
- Chaque ressource se charge, pendant son temps de cours, du suivi et de l'évaluation individuelle de la SAÉ pour la technologie utilisée en cours. Un ensemble de critères spécifiques à la SAÉ sont définis par chaque équipe de ressource.
- Pour faciliter les réunions avec le client, un créneau de SAÉ, commun à tous les étudiants, a été fixé tous les mardis matin de 8h à 10h.
- Des soutenances intermédiaires (pour les FI seulement) et terminales (pour les FI et les FA) serviront à l'évaluation du groupe dans sa globalité.
- Ainsi, chaque UE aura, en SAÉ, deux notes, une individuelle mise par la ou les ressource(s) et une globale mise par la ou les soutenance(s).
- Pour les FI, la soutenance intermédiaire (sur un créneau de 45 minutes) aura lieu en semaine 50 (le vendredi 20 décembre 2024), soit à peu près à 2/3 du projet, et devra permettre la présentation d'un prototype.
- La soutenance finale (sur un créneau de 90 minutes) aura lieu la semaine 13 (vendredi 4 avril 2025), juste avant le départ en deuxième période (FA) ou en stage (FI). Elle fait le bilan du projet et prend du recul sur les 6 compétences.

## Cadre de la SAÉ

Le cadre suivant a été défini :

![Architecture](cadre_SAE_2A.svg "Architecture générale de la SAÉ 2A")

Ce cadre permet d'intégrer les ressources à l'encadrement des SAÉ et l'évaluation des UE :

- Analyse du projet [R3.03] => UE1
- Réalisation de deux applications client (1 web en PHP [R3.01] et 1 mobile en Kotlin/Android [R4.A.11]) => UE1
- Chaque application doit consommer des données provenant d'une base de données **relationnelle** [R3.07, R4.01, R4.03] via un web service [R4.01] à écrire => UE3, UE4
- L'application web client, le web service et la base de données seront déployées sur un serveur [R3.05, R3.06] ou dans des conteneurs Docker [R4.A.08] => UE3
- Les applications devront mettre en valeur la qualité via l'utilisation de patrons de conception et l'application des principes S.O.L.I.D. [R3.04] => UE2
- Une application web d'administration simple des données sera ajoutée uniquement par les FI en Blazor [R3.01] => UE1
- Le code que vous produirez devra être testé [R4.02] => UE2
- La gestion de projet sera suivie en méthode SCRUM-lite [R3.10] => UE5
- Et bien sûr, la capacité à travailler en équipe sera évaluée => UE6

### Management de projet

L'équipe pédagogique gérant cette partie a pris soin de rédiger des documents précis qui vous permettront de connaître leurs attentes et spécifient les modalités d'évaluations. *Lisez-les bien !*

- [Attentes et consignes](Documents/GestionProjet_Consignes2A.pdf)
- [Grille d'auto-évaluation](Documents/GestionProjet_AutoEvaluation.pdf)
- [Répartition pour le suivi en management de projet](Documents/GestionProjet_RepartitionPourSuivi.pdf)

## Formation des équipes

* Vous êtes 94 étudiants et il y a 18 projets proposés
* Cela permet de faire 14 équipes de 5 étudiants et 4 équipes de 6 étudiants
* Les équipes de 5 étudiants sont à former au sein d'un même groupe de TP
* Il restera alors 3 ou 4 étudiants par groupe de TP qui permettront de former les 4 équipes de 6 étudiants
* Au S4, les groupes sont reformés du fait des colorations, mais les équipes perdurent

## Volume horaire

* Pour vous aider à faire vos planifications, nous vous fournissons le volume hebdomadaire des heures SAÉ sur l'année
* Les créneaux à l'emploi du temps sont communs pour les FI et FA :
  * Les FI doivent travailler pendant tous les créneaux, sauf les semaines estampillées :factory: (vacances) qui ne concernent que les FA.
  * Les FA doivent planifier l'utilisation de ces créneaux (sauf les semaines estampillées :briefcase: qui ne concernent que les FI) pour y dispatcher leur volume de SAÉ (vous devez travailler normalement 100h pour cette SAÉ, et n'oubliez pas que vous travaillez pendant les vacances de la Toussaint et de février, ça vous laisse donc 45h à dispatcher). Il est hors de question de les caler toutes au début ou à la fin de la SAÉ pour convenance personnelle. Vous les répartirez dans l'année. Vous avez plus de flexibilité, mais vous devez planifier tout ça au service du projet.

| S36 | S37 | S38 | S39 | S40 | S41 | S42 | S43 | :factory:S44 | S45 | S46 | S47 | :briefcase:S48 | :briefcase:S49 | :briefcase:S50 | :briefcase:S51 | :briefcase:S2 | :briefcase:S3 | S4 | S5 | S6 | S7 | S8 | :factory:S9 | S10 | S11 | S12 | S13 | S14 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | -- | -- | -- | -- | -- | -- | -- | -- | --- | --- | --- | --- | --- |
| 6h | 6h | 2h | 6h | 4h | 4h | 4h | 6h | 4x6h | 6h | 2h | 4h | 8h | 10h | 10h | 14h | 16h | 16h | 4h | 4h | 4h | 4h | 4h | 5x6h | 4h | 4h | 4h | 4h | 5h |

* Certaines ressources vous offrent aussi de leurs heures pour travailler votre SAÉ :
  + Analyse [R3.03] : 4h
  + PHP [R3.01] : 6,7h (les heures se diluent avec les sujets qui demandent de mettre en place un certain nombre de points dans votre projet, semaine par semaine)
  + Principes SOLID [R3.04] : 4x1h TD
  + Web API (PM) [R4.01] : 8h TP
  + Entity Framework (PM) [R4.01] : 8h TP
  + BD [R3.07, R4.03] : 0h mais possibilité de poser vos questions en TP


## Jalons

- mercredi 4 septembre 2024 : liste des équipes et leur projet transmises à [Laurent PROVOT](mailto:laurent.provot@uca.fr)
- fin de semaine 38 : date limite pour le rendu Analyse
- vendredi 4 octobre 2024 : rendu du premier rapport de gestion de projet
- dimanche 17 novembre 2024 : date limite pour rendu BD
- mardi 17 décembre 2024 : rendu du deuxième rapport de gestion de projet
- vendredi 20 décembre 2024 : première soutenance (FI)
- vendredi 4 avril 2025 : soutenance terminale (FI et FA)


## Frise des ressources

Afin d'aider les clients à avoir une meilleure idée de vos connaissances tout au long de vos SAÉ, voici une frise du déroulé de certaines ressources. Elle est donnée à titre indicatif. Il se peut donc que les ressources fassent quelques ajustements pédagogiques au cours de l'année. *Ce sont par conséquent les consignes et informations données par vos enseignants en ressources qui font foi !*

![](Documents/FriseRessources.svg "Frise du déroulé des ressources avec connaissances attendues")

* Les zones roses indiquent les périodes des vacances scolaires
* La zone bleue indique la période en entreprise pour les alternants
* Les rectangles rouges indiquent des évaluations en rapport avec vos SAÉ (quand l'info m'a été fournie par la ressource)

## Contacts

- Le client à contacter pour chaque projet est indiqué dans la page du projet (cf. [Liste des projets](#liste-des-projets))
- Le client **n'a pas** pour rôle de vous aider techniquement
- Pour les questions techniques, c'est votre enseignant de ressource que vous devez contacter et qui vous répondra
- Certains enseignants se proposent de répondre à vos questions si elles se rapportent à certains domaines qui pourraient ne pas apparaître dans vos ressources :
  + [Marc CHEVALDONNÉ](mailto:marc.chevaldonne@uca.fr) : Blazor, Web API (surtout en .NET), ORM (surtout en .NET), CI/CD
  + [Laurent PROVOT](mailto:laurent.provot@uca.fr) : Linux, C++, Kotlin
- Pour tout problème concernant le fonctionnement de la SAÉ, merci de contacter [Laurent PROVOT](mailto:laurent.provot@uca.fr)


## Liste des projets

1. [Tableau de bord](Projets/Projet_01.md)
2. [Fake news](Projets/Projet_02.md)
3. [SQLuedo](Projets/Projet_03.md)
4. [SQL Check](Projets/Projet_04.md)
5. [Cuisinote](Projets/Projet_05.md)
6. [Dékal](Projets/Projet_06.md)
7. [Qwirkle](Projets/Projet_07.md)
8. [Jeu de couverture par chemins dans les graphes](Projets/Projet_08.md)
9. [Gestion matériel IUT](Projets/Projet_09.md)
10. [Site support du cours Muscu Math](Projets/Projet_10.md)
11. [Suivi de formation pour sapeurs-pompiers](Projets/Projet_11.md)
12. [Réseau social des bricoleurs](Projets/Projet_12.md)
13. [À table !](Projets/Projet_13.md)
14. [Let's go!](Projets/Projet_14.md)
15. [Suivi des conventions de stage](Projets/Projet_15.md)
16. [Gestion de locations courte durée](Projets/Projet_16.md)
17. [Citations autour de la Fantasy](Projets/Projet_17.md)
18. [Chess2BUT](Projets/Projet_18.md)


## Affectations

1. Tableau de bord : (Laurent GIRAUD et Audrey POUCLET)

   - Ange BARDET (G7)
   - Marine GITTON (G7)
   - Lukas GRISAIL (G7)
   - Tom MILLERAT (G7)
   - Cyriaque SEGERIE (G7)

2. Fake news : (Laurent GIRAUD, Maxime PUYS et Isabelle GOI)

   - Jules BENIER (G3)
   - Gabriel CHATAIGNER (G3)
   - Maxime GALLI (G3)
   - Tom RAMBEAU (G3)
   - Johan VALENTIN (G3)

3. SQLuedo : (Anaïs DURAND et Neddra CHATTI)

   - Julien ARAUJO DA SILVA (G6)
   - Matéo BALKO (G6)
   - Louis BORDES (G6)
   - Titouan DUCROIX (G6)
   - Chahid LAFDEL (G6)

4. SQL Check : (Anaïs DURAND et Audrey POUCLET)

   - Louis GERMAIN (G7)
   - Christophe KENE (G7)
   - Simoni KHACHIDZE (G7)
   - Zayd NAHLI (G7)
   - Alicia RAGE (G7)

5. Cuisinote : (Anaïs DURAND et Isabelle GOI)

   - Vadim ABLANCOURT (G2)
   - Maxence GIRARDET (G2)
   - Benoît MÉTRAL-PUGET (G2)
   - Samuel PINTO (G2)
   - Alexis THOMAS (G2)

6. Dékal : (Pascal LAFOURCADE et Isabelle GOI)

   - Dylan ADAMOVIC (G5)
   - Mégane MARTIN (G5)
   - Rémi NEVEU (G5)
   - Damien NORTIER (G5)
   - Jean-Baptiste POIRSON (G6)
   - Loris PROTIN (G6)

7. Qwirkle : (Pascal LAFOURCADE et Neddra CHATTI)

   - Pierre DEMAY (G6)
   - Mathéo FOURNIE (G6)
   - Clément LESME (G6)
   - Gwenael PLANCHON (G6)

8. Jeu de couverture par chemins dans les graphes : (Florent FOUCAUD et Audrey POUCLET)

   - Timothée AUBRY (G4)
   - Tom BIARD (G4)
   - Kyllian CHABANON (G4)
   - Maxence GUITARD (G4)
   - Anas TOURS (G4)

9. Gestion matériel IUT : (Marc CHEVALDONNÉ et Neddra CHATTI)

   - Nicolas ARGAUT (G2)
   - Malak ATOUIL (G2)
   - Jean-Baptiste BOTTOU (G2)
   - Arthur GONZALEZ (G2)
   - Pauline PRADY (G2)

10. Site support du cours Muscu Math : (Adrien WOHRER et Audrey POUCLET)

    - Enzo BARREIRO (G5)
    - Théodore CHAILLOLEAU LECLERC (G5)
    - Timéo CHARREYRON (G5)
    - Lisa CHAUCHAT (G5)
    - Alexis DUMAIN (G5)

11. Suivi de formation pour sapeurs-pompiers : (Carine SIMON et Isabelle GOI)

    - Hugo CRENEAU (G5)
    - Hugo DELAFOUNOUX (G5)
    - Enzo GUBBIOTTI (G5)
    - Armand LECAILLE (G5)
    - Jeremy MOUYON (G5)

12. Réseau social des bricoleurs : (Cédric BOUHOURS et Isabelle GOI)

    - Tom BONNET (G3)
    - Thomas BOUSQUET (G3)
    - Yvan CALATAYUD (G3)
    - Titouan DA CUNHA (G3)
    - Flavien GOT BOUTET (G3)

13. À table ! : (Michelle CONRY-FIDONE et Neddra CHATTI)

    - David BERTRAND (G1)
    - Lucas DUFLOT (G1)
    - Lucas PAVET (G1)
    - Rémi PORTET (G1)
    - Leo SORLIN (G1)

14. Let's go! : (Michelle CONRY-FIDONE et Neddra CHATTI)

    - Yannis DOUMIR-FERNANDES (G6)
    - Auguste FLEURY (G6)
    - Mathéo HERSAN (G7)
    - Jules LASCRET (G7)
    - Rémi LAVERGNE (G7)
    - Paul LEVRAULT (G6)

15. Suivi des conventions de stage : (Christelle MOTTET et Isabelle GOI)

    - Celeste BARBOSA (G1)
    - Guillaume CHAMBAT (G1)
    - Ada MARTINEK (G1)
    - Camille TURPIN-ETIENNE (G1)
    - Ugo VIROLLE (G1)

16. Gestion de locations courte durée : (Neddra CHATTI et Audrey POUCLET)

    - Nolan DEVOUASSOUX (G4)
    - Cyprien FORGE (G3)
    - Thibaut HIRIART (G4)
    - Thibaud LA RIVIÈRE-GILLET (G3)
    - Lucas LHOMMET (G3)
    - Hugo SPERY (G4)

17. Citations autour de la Fantasy : (Matthieu RESTITUITO et Audrey POUCLET)

    - Leni BEAULATON (G4)
    - Kentin BRONGNIART (G4)
    - Kevin MONDEJA (G4)
    - Louis GUICHARD-MONTGUERS (G4)
    - Tommy NGUYEN (G4)
    - Maxime ROCHER (G4)

18. Chess2BUT : (Redha TAGUELMIMT et Neddra CHATTI)

    - Maxime ANDRÉ-MASSE (G2)
    - Lucas BODIN (G1)
    - Nathan CHALLET (G1)
    - Cédric CHARBONNEL (G2)
    - Thomas LEVADOUX (G2)
    - Erwan MECHOUD (G1)

